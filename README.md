# Overview

The Script displays visualisations of weather data, that comes from open data sets in https://pomiary.gdanskiewody.pl. 
In order to run visualisations the data for required years needs to be downloaded using the script with -d flag and start/end year specified (see usage below for details)

# Configuration

* In order to use the script for downloading data, replace a placeholder in the config.cfg:
 `APIKey = PUT_YOUR_API_KEY_HERE`
 with your API key. Register your account https://pomiary.gdanskiewody.pl/account/signup in order to obtain your API KEY

* Python prerequisites: python version: 3.8, external libraries to be installed: pandas, seaborn, matplotlib, numpy.

# Running the script
usage: weather_data_visualisations.py -d -s <start_year> -e <end_year> -y <year>

* -d - flag to indicate that data needs to be downloaded before displaying visualisations (note: sample data for years 2010- 2019 is available in `./data` directory so no need to download it again for those years)
* -s <start_year> - lower boundary of the data time span of the visualisations to be displayed/data to be downloaded
* -e <end_year> - upper boundary of the data time span of the visualisations to be displayed/data to be downloaded
* -y <year> - year for which drilled-down visualisation will be displayed 