import configparser


class Config:

    def __init__(self):
        parser = configparser.ConfigParser()
        parser.read("config.cfg")
        self.__api_key = parser['API']['APIKey']
        self.__measurements_endpoint = parser['API']['MeasurementsEndpoint']
        self.__stations_endpoint = parser['API']['StationsEndpoint']
        self.__data_directory = parser['FileSystem']['DataDir']
        self.__rain_file_name_suffix = parser['FileSystem']['RainLevelFileNameSuffix']
        self.__temperature_file_name_suffix = parser['FileSystem']['TemperatureFileNameSuffix']

    def get_api_key(self):
        return self.__api_key

    def get_measurements_endpoint(self):
        return self.__measurements_endpoint

    def get_stations_endpoint(self):
        return self.__stations_endpoint

    def get_data_directory(self):
        return self.__data_directory

    def get_rain_file_name_suffix(self):
        return self.__rain_file_name_suffix

    def get_temperature_file_name_suffix(self):
        return self.__temperature_file_name_suffix
