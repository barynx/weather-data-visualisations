from godapi import *
from config import *
import datetime
import pandas as pd


class FileIO:

    def __init__(self, config):
        self.__config = config

    def __build_filepath(self, year, station_no, measurement_type):
        file_name_suffix = ""
        if measurement_type == "Rain Level":
            file_name_suffix = self.__config.get_rain_file_name_suffix()
        elif measurement_type == "Temperature":
            file_name_suffix = self.__config.get_temperature_file_name_suffix()
        return str(self.__config.get_data_directory()) \
                                + '/' + str(year) \
                                + '/' + str(station_no) \
                                + str(file_name_suffix)

    def save_as_pandas_csv(self, daily_measurements_for_year_station, year, station_no, measurement_type):
        print("{} - Saving data for year: {} station: {}".format(datetime.datetime.now(), year, station_no))
        col_names = ['Date', 'Station', measurement_type]
        df = pd.DataFrame(data=daily_measurements_for_year_station, columns=col_names)
        filepath = self.__build_filepath(year, station_no, measurement_type)
        df.to_csv(path_or_buf=filepath, index=False)
        print("{} - Completed saving data".format(datetime.datetime.now()))

    def read_csv_as_pandas(self, year, station, measurement_type):
        filepath = self.__build_filepath(year, station, measurement_type)
        df = pd.read_csv(filepath)
        return df


class FileDownloader:

    def __init__(self, config, start_year, end_year):
        self.__config = config
        self.__start_year = start_year
        self.__end_year = end_year

    def download_daily_rain_levels(self):
        god_api_client = GODAPICLient(self.__config)
        file_io = FileIO(self.__config)
        stations = god_api_client.get_stations()
        rain_station_nos = [station["no"] for station in stations["data"] if station["rain"]]
        print("Stations to download the data from: " + str(rain_station_nos))

        current_year = self.__start_year

        while current_year <= self.__end_year:
            for rain_station_no in rain_station_nos:
                start_date = datetime.date(current_year, 1, 1)
                end_date = datetime.date(current_year, 12, 31)
                print("{} - Retrieving data for year: {} station: {}".format(datetime.datetime.now(), current_year, rain_station_no))
                daily_rain_levels = god_api_client.get_daily_sums_between_dates(start_date, end_date, rain_station_no, "rain")
                print("{} - Completed retrieving data".format(datetime.datetime.now()))
                file_io.save_as_pandas_csv(daily_rain_levels, current_year, rain_station_no, "Rain Level")
            current_year = current_year + 1

    def download_avg_daily_temperatures(self):
        god_api_client = GODAPICLient(self.__config)
        file_io = FileIO(self.__config)
        stations = god_api_client.get_stations()
        temp_station_nos = [station["no"] for station in stations["data"] if station["temp"]]
        print("Stations to download the data from: " + str(temp_station_nos))

        current_year = self.__start_year

        while current_year <= self.__end_year:
            for temp_station_no in temp_station_nos:
                start_date = datetime.date(current_year, 1, 1)
                end_date = datetime.date(current_year, 12, 31)
                print("{} - Retrieving data for year: {} station: {}".format(datetime.datetime.now(), current_year, temp_station_no))
                daily_rain_levels = god_api_client.get_daily_averages_between_dates(start_date, end_date, temp_station_no, "temp")
                print("{} - Completed retrieving data".format(datetime.datetime.now()))
                file_io.save_as_pandas_csv(daily_rain_levels, current_year, temp_station_no, "Temperature")
            current_year = current_year + 1
