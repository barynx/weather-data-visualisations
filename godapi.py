import urllib.request
import json
import datetime
import configparser


class GODAPICLient:

    def __init__(self, config):
        self.__config = config

    def __build_url_for_measurements(self, measurement, date, station_no):
        return str(self.__config.get_measurements_endpoint()) \
            + "/" \
            + str(station_no) \
            + "/" \
            + str(measurement) \
            + "/" \
            + str(date)

    def __get_json_response(self, url):
        request = urllib.request.Request(url, method='GET')
        request.add_header("Authorization", "Bearer " + str(self.__config.get_api_key()))
        response = urllib.request.urlopen(request)
        data = response.read().decode('utf-8')
        return json.loads(data)

    def __get_measurement_for_date(self, measurement_type, date, station_no):
        url = self.__build_url_for_measurements(measurement_type, date, station_no)
        return self.__get_json_response(url)

    def get_stations(self):
        url_stations = self.__config.get_stations_endpoint()
        return self.__get_json_response(url_stations)

    def get_daily_sum_for_date(self, measurement_type, date, station_no):
        measurement = self.__get_measurement_for_date(measurement_type, date, station_no)

        sum_daily_measurement_level = 0
        for measurement_level in measurement["data"]:
            level = measurement_level[1]
            if level is not None:
                sum_daily_measurement_level = sum_daily_measurement_level + level
        return sum_daily_measurement_level

    def get_daily_sums_between_dates(self, start, end, station_no, measurement_type):
        delta_one = datetime.timedelta(days=1)
        daily_measurement_levels = []

        while end >= start:
            date_formatted = start.strftime("%Y-%m-%d")
            sum_daily_measurement_level = self.get_daily_sum_for_date(measurement_type, date_formatted, station_no)
            daily_measurement_levels.append((date_formatted, station_no, sum_daily_measurement_level))
            start = start + delta_one

        return daily_measurement_levels

    def get_daily_average_for_date(self, measurement_type, date, station_no):
        measurement = self.__get_measurement_for_date(measurement_type, date, station_no)

        sum_daily_measurement_level = 0
        count = 0
        for measurement_level in measurement["data"]:
            level = measurement_level[1]
            if level is not None:
                sum_daily_measurement_level = sum_daily_measurement_level + level
                count = count + 1
        if count != 0:
            daily_avg = sum_daily_measurement_level/count
        else:
            daily_avg = -273
        return daily_avg

    def get_daily_averages_between_dates(self, start, end, station_no, measurement_type):
        delta_one = datetime.timedelta(days=1)
        daily_avg_measurement_levels = []

        while end >= start:
            date_formatted = start.strftime("%Y-%m-%d")
            avg_daily_measurement = self.get_daily_average_for_date(measurement_type, date_formatted, station_no)
            daily_avg_measurement_levels.append((date_formatted, station_no, avg_daily_measurement))
            start = start + delta_one

        return daily_avg_measurement_levels
