from files import *
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


class DataVisualizer:

    def __init__(self, config):
        self.__config = config

    def __build_data_for_monthly_rain_levels_heatmap(self, stations, year):
        rain_heatmap = np.empty([1, 12])
        file_io = FileIO(self.__config)

        for station in stations:
            df = file_io.read_csv_as_pandas(year, station, "Rain Level")
            df_date_rain = df[['Date', 'Rain Level']]
            df_date_rain['Date'] = pd.to_datetime(df_date_rain['Date'])
            dg = df_date_rain.groupby(pd.Grouper(key='Date', freq='1M')).sum()
            rain_level = dg.loc[:, 'Rain Level'].values
            rain_heatmap = np.vstack([rain_heatmap, rain_level])

        rain_heatmap = np.delete(rain_heatmap, 0, 0)
        return rain_heatmap

    def __build_data_for_monthly_rainy_days_heatmap(self, stations, year):
        rain_heatmap = np.empty([1, 12])
        file_io = FileIO(self.__config)

        for station in stations:
            df = file_io.read_csv_as_pandas(year, station, "Rain Level")
            df_date_rain = df[['Date', 'Rain Level']]
            rainy_day_filter = df_date_rain['Rain Level'] > 0.5
            df_date_rain_filtered = df_date_rain[rainy_day_filter]

            df_date_rain_filtered['Date'] = pd.to_datetime(df_date_rain_filtered['Date'])
            dg = df_date_rain_filtered.groupby(pd.Grouper(key='Date', freq='1M')).count()
            rain_level = dg.loc[:, 'Rain Level'].values
            rain_heatmap = np.vstack([rain_heatmap, rain_level])

        rain_heatmap = np.delete(rain_heatmap, 0, 0)
        return rain_heatmap

    def __build_data_for_monthly_temperatures_heatmap(self, stations, year):
        temperature_heatmap = np.empty([1, 12])
        file_io = FileIO(self.__config)

        for station in stations:
            df = file_io.read_csv_as_pandas(year, station, "Temperature")
            df_date_temp = df[['Date', 'Temperature']]

            df_date_temp['Date'] = pd.to_datetime(df_date_temp['Date'])
            dg = df_date_temp.groupby(pd.Grouper(key='Date', freq='1M')).mean()
            temperature = dg.loc[:, 'Temperature'].values
            temperature_heatmap = np.vstack([temperature, temperature])

        temperature_heatmap = np.delete(temperature_heatmap, 0, 0)
        return temperature_heatmap

    def __build_data_for_monthly_averages_heatmap(self, start_year, end_year, stations, measurement_type):
        current_year = start_year
        avg_measurements_heatmap = np.empty([1, 12])

        while current_year <= end_year:
            if measurement_type == 'rainy days':
                measurements_per_month = self.__build_data_for_monthly_rainy_days_heatmap(stations[current_year], current_year)
            elif measurement_type == 'rain levels [mm]':
                measurements_per_month = self.__build_data_for_monthly_rain_levels_heatmap(stations[current_year], current_year)
            elif measurement_type == 'temperature [deg C]':
                measurements_per_month = self.__build_data_for_monthly_temperatures_heatmap(stations[current_year],
                                                                                           current_year)
            else:
                measurements_per_month = None
            avg_measurements_per_month = np.mean(measurements_per_month, axis=0)
            avg_measurements_heatmap = np.vstack([avg_measurements_heatmap, [avg_measurements_per_month]])
            current_year = current_year + 1

        avg_measurements_heatmap = np.delete(avg_measurements_heatmap, 0, 0)

        avg_of_avg = np.mean(avg_measurements_heatmap, axis=0)
        avg_measurements_heatmap = np.vstack([avg_measurements_heatmap, [avg_of_avg]])

        return avg_measurements_heatmap

    def __show_monthly_heatmap(self, data, title, y_ticks, y_label):
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        plt.figure(figsize=(15, 10))
        sns.heatmap(data,
                    annot=True,
                    fmt=".4g",
                    cmap='Blues',
                    yticklabels=y_ticks,
                    xticklabels=months)
        plt.xlabel('Month')
        plt.ylabel(y_label)
        plt.title(title)
        plt.show()

    def show_monthly_averages(self, start_year, end_year, stations, measurement_type):
        monthly_averages = self.__build_data_for_monthly_averages_heatmap(start_year,
                                                                          end_year,
                                                                          stations,
                                                                          measurement_type)

        self.__show_monthly_heatmap(monthly_averages,
                                    'Monthly average of ' + measurement_type + ' in Gdańsk in years ' + str(
                                        start_year) + '-' + str(end_year),
                                    list(range(start_year, end_year + 1)) + ['AVG'],
                                    'Year')

    def show_monthly_rain_levels(self, year, stations):
        included_stations = stations[year]
        included_stations.sort()
        rain_levels_per_month = self.__build_data_for_monthly_rain_levels_heatmap(included_stations, year)
        self.__show_monthly_heatmap(rain_levels_per_month,
                                    'Monthly rain levels [mm] in ' + str(year) + ' in Gdańsk per weather station',
                                    included_stations,
                                    'Weather station #')

    def show_monthly_number_rainy_days(self, year, stations):
        included_stations = stations[year]
        included_stations.sort()
        number_rainy_days_per_month = self.__build_data_for_monthly_rainy_days_heatmap(included_stations, year)
        self.__show_monthly_heatmap(number_rainy_days_per_month,
                                    'Monthly number of rainy days in ' + str(year) + ' in Gdańsk per weather station',
                                    included_stations,
                                    'Weather station #')
