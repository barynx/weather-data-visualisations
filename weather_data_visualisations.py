from files import *
from heatmaps import *
from config import *
import sys
import getopt


def main(argv):

    download = False
    start_year = 0
    end_year = 0
    year = 0

    config = Config()

    gdansk_temp_stations = {
        2017: [31],
        2018: [31],
        2019: [31, 307],
        2020: [31, 307]
    }

    gdansk_rain_stations = {
        2010: [1, 7, 8, 9, 313],
        2011: [1, 7, 8, 9, 10],
        2012: [1, 2, 7, 9, 10, 313],
        2013: [1, 8, 9, 10, 313],
        2014: [2, 10, 313],
        2015: [2, 9, 10, 313],
        2016: [1, 2, 9, 8, 16, 12, 11, 10, 7, 15],
        2017: [1, 35, 2, 9, 8, 31, 16, 12, 11, 10, 7, 15, 35],
        2018: [310, 303, 2, 309, 313, 307, 9, 306, 308, 305, 18, 311, 8, 31, 16, 12, 301, 11, 10, 7, 15],
        2019: [1, 310, 35, 303, 302, 2, 309, 313, 307, 9, 306, 308, 305, 18, 311, 8, 31, 16, 12, 304, 301, 11, 7, 15],
        2020: [1, 310, 35, 303, 302, 2, 309, 313, 307, 9, 306, 308, 305, 18, 311, 8, 31, 16, 12, 304, 301, 11, 10, 7, 15]
    }

    try:
        opts, args = getopt.getopt(argv, "hds:e:y:")
    except getopt.GetoptError:
        print('usage: weather_data_visualisations.py -d -s <start_year> -e <end_year> -y <year>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('usage: weather_data_visualisations.py -d -s <start_year> -e <end_year> -y <year>')
            sys.exit()
        elif opt == '-d':
            download = True
        elif opt == '-s':
            start_year = int(arg)
        elif opt == '-e':
            end_year = int(arg)
        elif opt == '-y':
            year = int(arg)

    if download:
        file_downloader = FileDownloader(config, start_year, end_year)
        file_downloader.download_daily_rain_levels()
        file_downloader.download_avg_daily_temperatures()

    if start_year != 0 and end_year != 0:
        data_visualizer = DataVisualizer(config)
        data_visualizer.show_monthly_averages(start_year, end_year, gdansk_rain_stations, "rain levels [mm]")
        data_visualizer.show_monthly_averages(start_year, end_year, gdansk_rain_stations, "rainy days")
        data_visualizer.show_monthly_averages(start_year, end_year, gdansk_temp_stations, "temperature [deg C]")

    if year != 0:
        data_visualizer = DataVisualizer(config)
        data_visualizer.show_monthly_rain_levels(year, gdansk_rain_stations)
        data_visualizer.show_monthly_number_rainy_days(year, gdansk_rain_stations)


if __name__ == "__main__":
    main(sys.argv[1:])
